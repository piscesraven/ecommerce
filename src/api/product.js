import Axios from './config'

export default {
  getAllLsit(data) {
    return Axios({
      url: `/api/${process.env.VUE_APP_CUSTOMPATH}/admin/products`,
      method: 'get',
      data
    })
  },
  addList(data) {
    return Axios({
      url: `/api/${process.env.VUE_APP_CUSTOMPATH}/admin/product`,
      method: 'post',
      data: {
        data
      }
    })
  },
  getLsitPage(page) {
    return Axios({
      url: `/api/${process.env.VUE_APP_CUSTOMPATH}/admin/products?page=${page}`,
      method: 'get',

    })
  },
  editList(id, data) {
    return Axios({
      url: `/api/${process.env.VUE_APP_CUSTOMPATH}/admin/product/${id}`,
      method: 'put',
      data
    })
  },
  listData(data, method, id) {
    return Axios({
      url: `/api/${process.env.VUE_APP_CUSTOMPATH}/admin/product${id}`,
      method: method,
      data: {
        data
      }
    })
  }

}