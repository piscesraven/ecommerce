import AXIOS from './config';
// import axios from 'axios'

export default {
  loginIn(data) {
    return AXIOS({
      url: `/admin/signin`,
      method: 'post',
      data
    })
  },
  loginOut() {
    return AXIOS({
      url: `/logout`,
      method: 'post',
    })
  }

}
