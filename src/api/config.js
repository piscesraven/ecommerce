import axios from 'axios';

const service = axios.create({
  baseURL: `${process.env.VUE_APP_APIPATH}`,
  timeout: 10000
});

export default service