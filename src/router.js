import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import DashBoard from './components/backstage/DashBoard.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: "*",
    redirect: 'login'
  },
  {
    path: '/',
    name: 'home',
    component: Home,
    meta: { requiresAuth: true }
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('./components/pages/login.vue')
  },
  {
    path: '/dashboard',
    name: 'DashBoard',
    component: DashBoard,
    children: [
      {
        path: '',
        name: 'Products',
        component: () => import('./components/backstage/Products.vue'),
        meta: { requiresAuth: true },

      }

    ]
  },
  ]
})
