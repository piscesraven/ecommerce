import Vue from 'vue'
import state from './vuex/state'
import mutations from './vuex/mutations'
import Vuex from 'vuex'


Vue.use(Vuex)

export default new Vuex.Store({
  state,
  mutations
})
