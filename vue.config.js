module.exports = {
  configureWebpack: {
    resolve: {
      alias: {
        '@as': '@/assets',
        '@c': '@/components',
        '@v': '@/views',
        '@img': '@/assets/img',
        '@css': '@/assets/scss',
        '@api': '@/api',
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        data: ` @import "@css/global.scss";`
      }
    }
  },
}